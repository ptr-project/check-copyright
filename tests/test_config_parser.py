# Copyright (c) 2020 Kai Hoewelmeyer
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import unittest
import tempfile
import contextlib
import os

from check_copyright.check_copyright import read_config

# Inspiration https://stackoverflow.com/a/57701186/522251
@contextlib.contextmanager
def temporary_config_file(config_content):
    try:
        f = tempfile.NamedTemporaryFile(delete=False)
        tmp_name = f.name
        f.write(config_content.encode('UTF-8'))
        f.close()
        yield tmp_name
    finally:
        os.unlink(tmp_name)

class ConfigParserTest(unittest.TestCase):

    def test_full_config(self):
        config_content = """
        [check-copyright]
        patterns = *.py, *.cpp
        exclude = folder/*
        license_file = LIC
        """

        with temporary_config_file(config_content) as filename:
            config = read_config([filename])
            self.assertEqual(config['extensions'], ['*.py', '*.cpp'])
            self.assertEqual(config['exclude_patterns'], ['folder/*'])
            self.assertEqual(config['license_file'], 'LIC')

    def test_default_values(self):
        config = read_config([])

        self.assertEqual(config['extensions'], ['*.cpp', '*.h', '*.py', '*.sh'])
        self.assertEqual(config['exclude_patterns'], [])
        self.assertEqual(config['license_file'], 'LICENSE')

    def test_empty_exclude_patterns(self):
        config_content = """
        [check-copyright]
        exclude =
        """

        with temporary_config_file(config_content) as filename:
            config = read_config([filename])
            self.assertEqual(config['exclude_patterns'], [])

